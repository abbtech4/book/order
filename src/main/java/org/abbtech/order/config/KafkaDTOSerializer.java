package org.abbtech.order.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.abbtech.order.dto.KafkaDTO;
import org.apache.kafka.common.serialization.Serializer;

public class KafkaDTOSerializer implements Serializer<KafkaDTO> {
    private final ObjectMapper objectMapper = new ObjectMapper();


    @Override
    public byte[] serialize(String topic, KafkaDTO data) {
        try {
            return objectMapper.writeValueAsBytes(data);
        } catch (Exception e) {
            throw new RuntimeException("Error serializing KafkaDTO", e);
        }
    }

    @Override
    public void close() {

    }
}
