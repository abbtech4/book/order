package org.abbtech.order.config;

import org.abbtech.order.client.ExceptionDecode;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeignConfig {

    @Bean
    public ExceptionDecode exceptionDecode(){
        return new ExceptionDecode();
    }
}
