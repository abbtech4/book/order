package org.abbtech.order.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Entity
@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue(generator = "UUID")
    private UUID id;
    @Column(name = "bookid")
    private UUID bookId;

    @Column(name = "quantity")
    private int quantity;

    @Column(name = "userid")
    private UUID userId;


}
