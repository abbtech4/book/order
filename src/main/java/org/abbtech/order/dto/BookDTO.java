package org.abbtech.order.dto;

import jakarta.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@Data
@AllArgsConstructor
public class BookDTO {
    private String title;
    private String author;
    private BigDecimal price;
}
