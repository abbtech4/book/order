package org.abbtech.order.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

//burdan bashda
@Builder
@Getter
@Setter
public class KafkaDTO {
    private UUID orderId;
    private String messages;
    private UUID userId;
    private String userEmail;
}
