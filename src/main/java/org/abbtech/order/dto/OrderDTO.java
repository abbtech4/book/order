package org.abbtech.order.dto;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;

import java.util.UUID;


@Builder
@Getter
@Data
public class OrderDTO {
    private int quantity;
    private UUID bookId;
}
