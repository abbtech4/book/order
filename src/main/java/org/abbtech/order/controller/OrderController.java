package org.abbtech.order.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.abbtech.order.dto.BookDTO;
import org.abbtech.order.dto.OrderDTO;
import org.abbtech.order.service.OrderService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/order")
@RequiredArgsConstructor
@Tag(name = "order for Only USER roles",description = "creating confirming and getting order")
public class OrderController {
    private final OrderService orderService;

    @Operation(summary = "creating order for a book it can throw exception if book doesnt exists.Exception handled")
    @PostMapping("/create")
    public ResponseEntity<BookDTO>createOrder(@RequestHeader(name = "x-user-id") String userId,
                                              @RequestHeader(name = "x-user-email") String userEmail,
                                              @RequestBody OrderDTO orderDTO){
    return new ResponseEntity<>(orderService
            .createOrder(orderDTO,userId,userEmail),HttpStatus.CREATED);
    }

    @Operation(summary = "confirming order by its id, if no such order exits it will throw exception. It also handled")
    @GetMapping("/confirm/{id}")
    public ResponseEntity<String> confirmOrder(@PathVariable UUID id){
        return new ResponseEntity<>(orderService.confirmOrder(id),HttpStatus.OK);
    }

    @Operation(summary = "getting order by its id if no such order exits it will throw exception. It also handled")
    @GetMapping("/get/{id}")
    public ResponseEntity<OrderDTO> getOrderByID(@PathVariable UUID id){
        return new ResponseEntity<>(orderService.getOrderById(id), HttpStatus.OK);
    }

}
