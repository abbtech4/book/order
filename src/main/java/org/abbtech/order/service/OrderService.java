package org.abbtech.order.service;

import lombok.RequiredArgsConstructor;
import org.abbtech.order.client.BookServiceClient;
import org.abbtech.order.dto.BookDTO;
import org.abbtech.order.dto.KafkaDTO;
import org.abbtech.order.dto.OrderDTO;
import org.abbtech.order.exception.BookNotFoundException;
import org.abbtech.order.exception.NoSuchOrderException;
import org.abbtech.order.model.Order;
import org.abbtech.order.repository.OrderRepository;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class OrderService {
    private final OrderRepository orderRepository;
    private final KafkaTemplate<String,Object> kafkaTemplate;
    private final BookServiceClient bookServiceClient;
    public OrderDTO getOrderById(UUID id){
       Order order = orderRepository.findById(id).orElseThrow(()-> new NoSuchOrderException("Couldn't find such order"));

       OrderDTO orderDTO = OrderDTO.builder()
               .bookId(order.getBookId())
               .quantity(order.getQuantity())
               .build();

       return orderDTO;
    }

    public String confirmOrder(UUID id){
        Order order = orderRepository.findById(id).orElseThrow(()-> new NoSuchOrderException("Couldn't find such order"));
        return "order confirmed";
    }


    @Transactional
    public BookDTO createOrder(OrderDTO orderDTO,String userId,String userEmail){
        //exception ata biler
        BookDTO bookDTO = bookServiceClient.getBook(orderDTO.getBookId());
        if(bookDTO==null){
            throw new BookNotFoundException("no such book on our store");
        }
        Order order = Order.builder()
                .bookId(orderDTO.getBookId())
                .userId(UUID.fromString(userId))

                .quantity(orderDTO.getQuantity())
                .build();
        orderRepository.save(order);
        KafkaDTO kafkaDTO = KafkaDTO.builder()
                .orderId(order.getId())
                .userEmail(userEmail)
                .userId(order.getUserId())
                .messages("confirm your order: "+ order.getId())
                .build();
        kafkaTemplate.send("NotificationTopic",kafkaDTO);
        return bookDTO;
    }

}
