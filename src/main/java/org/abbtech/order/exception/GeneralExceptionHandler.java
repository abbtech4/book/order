package org.abbtech.order.exception;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GeneralExceptionHandler {
    @ExceptionHandler(BookNotFoundException.class)
    public ResponseEntity<String> handle(BookNotFoundException bookNotFoundException){
        return new ResponseEntity<>(bookNotFoundException.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(NoSuchOrderException.class)
    public ResponseEntity<String> handle(NoSuchOrderException noSuchTaskException){
        return new ResponseEntity<>(noSuchTaskException.getMessage(),HttpStatus.NOT_FOUND);
    }
}
