package org.abbtech.order.client;

import feign.Response;
import feign.codec.ErrorDecoder;
import org.abbtech.order.exception.BookNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
public class ExceptionDecode implements ErrorDecoder {
    @Override
    public Exception decode(String methodKey, Response response) {
        HttpStatus status = HttpStatus.valueOf(response.status());

        switch (status){
            case NOT_FOUND -> {
                return new BookNotFoundException("couldnt find book from book service");
            }
            case INTERNAL_SERVER_ERROR -> {
                return new BookNotFoundException("External error");
            }default -> {
                return new BookNotFoundException("Something happened in BOOK-MS");
            }
        }
    }
}
