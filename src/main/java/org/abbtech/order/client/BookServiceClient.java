package org.abbtech.order.client;

import org.abbtech.order.dto.BookDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


import java.util.UUID;

@FeignClient(name = "book-service-client",path = "/book",url = "http://localhost:8081")
public interface BookServiceClient {
    @GetMapping("/get/{id}")
     BookDTO getBook(@PathVariable UUID id);
}
